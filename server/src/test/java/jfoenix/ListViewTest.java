package jfoenix;

import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.controls.JFXListView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * Created by ldh on 2019/2/12.
 */
public class ListViewTest extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        JFXListView<Label> list = new JFXListView<Label>();
        for(int i = 0 ; i < 4 ; i++) list.getItems().add(new Label("Item " + i));
        list.getStyleClass().add("/css/mylistview");

        JFXDecorator decorator = new JFXDecorator(primaryStage, list);
        decorator.setCustomMaximize(true);
        Scene scene = new Scene(decorator, 800, 650);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
