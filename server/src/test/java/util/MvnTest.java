package util;

import ldh.maker.util.MvnUtil;
import org.apache.maven.shared.invoker.*;

public class MvnTest {

    public static void main(String[] args) throws MavenInvocationException {
        MvnUtil.runMvn("clean spring-boot:run", "E:\\git\\maker\\code\\school\\school-jsp\\pom.xml", (s, t)->{
            System.out.println("msg:" + s);
            if (t != null) {
                t.printStackTrace();
            }
        });
    }
}
