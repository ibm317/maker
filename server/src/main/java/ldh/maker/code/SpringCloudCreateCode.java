package ldh.maker.code;

import ldh.maker.freemaker.ApplicationBootMaker;
import ldh.maker.freemaker.ApplicationPropertiesMaker;
import ldh.maker.freemaker.PomXmlMaker;
import ldh.maker.util.CopyDirUtil;
import ldh.maker.util.FileUtil;
import ldh.maker.vo.EurekaServerParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpringCloudCreateCode {

    public static String EurekaServer = "EurekaServer";

    public void createEurekaServer(EurekaServerParam eurekaServerParam) {
        String resourcePath = createResourcePath(EurekaServer);
        new ApplicationPropertiesMaker()
                .projectRootPackage(eurekaServerParam.getProjectPackage())
                .ftl("sc/EurekaServerBootstrapYml.ftl")
                .fileName("bootstrap.yml")
                .put("param", eurekaServerParam)
                .outPath(resourcePath)
                .make();

        resourcePath = createPath(EurekaServer, eurekaServerParam.getProjectPackage());
        new ApplicationBootMaker()
                .projectRootPackage(eurekaServerParam.getProjectPackage())
                .ftl("sc/EurekaServerApplicationBoot.ftl")
                .fileName("EurekaServerApplication.java")
                .outPath(resourcePath)
                .make();

//        resourcePath = createPath(EurekaServer, projectRootPackage+".config");
//        new ApplicationBootMaker()
//                .projectRootPackage(projectRootPackage)
//                .ftl("sc/BasicAuthenication.ftl")
//                .fileName("BasicAuthenication.java")
//                .outPath(resourcePath)
//                .make();
//
//        new ApplicationBootMaker()
//                .projectRootPackage(projectRootPackage)
//                .ftl("sc/WebSecurityConfig.ftl")
//                .fileName("WebSecurityConfig.java")
//                .outPath(resourcePath)
//                .make();

        resourcePath = createPomPath(EurekaServer);
        new PomXmlMaker()
                .projectRootPackage(eurekaServerParam.getProjectPackage())
                .ftl("sc/EurekaServerPom.ftl")
                .outPath(resourcePath)
                .make();
    }

    public void createZuul() {

    }

    public void createConfigServer() {

    }

    protected String createResourcePath(String projectName) {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", "springcloud",  projectName, "src", "main", "resources"));
        path = makePath(path, dirs);
        return path;
    }

    protected String makePath(String path, List<String> dirs) {
        return CopyDirUtil.makePath(path, dirs);
    }

    protected String createPath(String projectName, String pk) {
        String path = FileUtil.getSourceRoot();
        String[] pks = pk.split("\\.");
        List<String> dirs = new ArrayList<>(Arrays.asList("code", "springcloud", projectName, "src", "main", "java"));
        for (String p : pks) {
            dirs.add(p);
        }
        path = makePath(path, dirs);
        return path;
    }

    protected String createPomPath(String projectName) {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", "springcloud", projectName));
        path = makePath(path, dirs);
        return path;
    }
}
