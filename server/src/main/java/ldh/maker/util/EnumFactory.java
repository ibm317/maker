package ldh.maker.util;

import ldh.maker.freemaker.EnumStatusMaker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ldh on 2017/4/19.
 */
public class EnumFactory {

    private static EnumFactory instance;
    private Map<String, EnumStatusMaker> enumStatusMakerMap = new HashMap<>();

    public static EnumFactory getInstance() {
        if (instance == null) {
            synchronized (EnumFactory.class) {
                if (instance == null) {
                    instance = new EnumFactory();
                }
            }
        }
        return instance;
    }

    public EnumStatusMaker get(String key) {
        return enumStatusMakerMap.get(key);
    }

    public void put(String key, EnumStatusMaker maker) {
        enumStatusMakerMap.put(key, maker);
    }

    public Map<String, EnumStatusMaker> getEnumStatusMakerMap() {
        return enumStatusMakerMap;
    }
}
