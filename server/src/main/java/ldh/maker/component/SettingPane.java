package ldh.maker.component;

import com.jfoenix.controls.JFXTabPane;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/3.
 */
public class SettingPane extends JFXTabPane {

    protected TreeItem<TreeNode> treeItem;
    protected String dbName;
    private PackagePane packagePane;
    private AliasTablePane aliasTablePane;
    protected TableNoPane noTablePane;

    public SettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        this.treeItem = treeItem;
        this.dbName = dbName;

        this.setSide(Side.TOP);

        initTab();
    }

    protected void initTab() {
        packagePane = buildPackagePaneTab();
        buildNoTablePaneTab();
        buildAliasPaneTab();
    }

    public boolean isSetting() {
        return packagePane == null ? true : packagePane.isSetting();
    }

    protected PackagePane buildPackagePaneTab() {
        PackagePane packagePane = new PackagePane(treeItem, dbName);
        createTab("路径设置", packagePane);
        return packagePane;
    }

    protected void buildNoTablePaneTab() {
        noTablePane = new TableNoPane(treeItem, dbName);
        Tab tab = createTab("不生成的Table", noTablePane);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                noTablePane.show();
            }
        });
    }

    protected void buildAliasPaneTab() {
        aliasTablePane = new AliasTablePane(treeItem, dbName);
        Tab tab = createTab("别名设置", aliasTablePane);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                aliasTablePane.show();
            }
        });
    }

    protected Tab createTab(String tabName, Node node) {
        Tab tab = new Tab(tabName);
        tab.setContent(node);
        tab.setClosable(false);
        this.getTabs().add(tab);
        return tab;
    }

}
