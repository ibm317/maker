package ${projectRootPackage};

import ldh.common.testui.TestUIMainApp;
import ${projectRootPackage}.ApplicationBoot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class TestUiServiceTest {

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(ApplicationBoot.class, args);
        TestUIMainApp.start(args, applicationContext);
    }
}
