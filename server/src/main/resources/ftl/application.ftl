spring:
  profiles:
    active: dev

  application:
    name: ${projectName}

  datasource:
    name: hikariDataSource
    type: com.zaxxer.hikari.HikariDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://${dbConnectionData.ipProperty}:${dbConnectionData.portProperty?c}/${dbName}?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true&zeroDateTimeBehavior=CONVERT_TO_NULL
    username: ${dbConnectionData.userNameProperty}
    password: ${dbConnectionData.passwordProperty}
    hikari:
      minimum-idle: 5
      maximum-pool-size: 15
      auto-commit: true
      idle-timeout: 30000
      pool-name: DatebookHikariCP
      max-lifetime: 1800000
      connection-timeout: 30000
      connection-test-query: SELECT 1

  mvc:
    view:
      prefix: /WEB-INF/jsp/
      suffix: .jsp
    static-path-pattern: /**

  resources:
    static-locations: classpath:/resource/

server:
  port: 8282