package ldh.maker;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ldh.maker.util.UiUtil;

public class SpringCloudCreateMain extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        UiUtil.STAGE = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/SpringCloudBuilder.fxml"));

        Scene scene = new Scene(root);
//        scene.getStylesheets().add("/styles/Main.css");
        scene.getStylesheets().add("/styles/bootstrapfx.css");
        scene.getStylesheets().add("/styles/java-keywords.css");
        scene.getStylesheets().add("/styles/xml-highlighting.css");
        scene.getStylesheets().add(SpringCloudCreateMain.class.getResource("/styles/jfoenix-components.css").toExternalForm());

        stage.setTitle("智能代码生成器之生成spring cloud 代码");
        stage.setScene(scene);
        stage.show();
    }
}
