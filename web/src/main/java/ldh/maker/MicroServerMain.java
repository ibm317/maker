package ldh.maker;

import ldh.maker.ServerMain;
import ldh.maker.component.VertxContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh123 on 2018/6/23.
 */
public class MicroServerMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setType("micro");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成spring boot api代码 ";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}
