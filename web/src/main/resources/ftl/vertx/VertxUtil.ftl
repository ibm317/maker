package ${beanPackage};

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;
import freemarker.template.TemplateHashModel;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.common.template.TemplateEngine;
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine;
import ldh.common.util.PaginationUtil;
import java.lang.reflect.Field;

/**
* @author: ${Author}
* @date: ${DATE}
*/
public class VertxUtil {

    private static volatile Vertx vertx;
    private static volatile Router router;
    private static volatile FreeMarkerTemplateEngine freeMarkerTemplateEngine = null;

    public static String CONSUME_TYPE = "_consume_type";

    public static void setVertx(Vertx vertx) {
        VertxUtil.vertx = vertx;
    }

    public static Vertx getVertx() {
        return VertxUtil.vertx;
    }

    public static void setRouter(Router router) {
        VertxUtil.router = router;
    }

    public static Router getRouter() {
        return VertxUtil.router;
    }

    public static TemplateEngine getTemplateEngine(Vertx vertx) {
        if (freeMarkerTemplateEngine == null) {
            synchronized (VertxUtil.class) {
                if (freeMarkerTemplateEngine == null) {
                    freeMarkerTemplateEngine = FreeMarkerTemplateEngine.create(vertx);
                    Configuration config = (Configuration) getFieldValueByFieldName("config", freeMarkerTemplateEngine);
                    if (config != null) {
                        setSharedVariable(config);
                    }
                }
            }
        }
        return freeMarkerTemplateEngine;
    }

    public static void json(RoutingContext ctx) {
        ctx.put(CONSUME_TYPE, "json");
    }

    public static void html(RoutingContext ctx) {
        ctx.put(CONSUME_TYPE, "html");
    }

    private static Object getFieldValueByFieldName(String fieldName, Object object) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            //设置对象的访问权限，保证对private的属性的访问
            return field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void setSharedVariable(Configuration configuration){
        BeansWrapperBuilder builder = new BeansWrapperBuilder(freemarker.template.Configuration.VERSION_2_3_23);
        builder.setUseModelCache(true);
        builder.setExposeFields(true);

        configuration.setClassicCompatible(true);
        configuration.setNumberFormat("#");

        // Get the singleton:
        BeansWrapper beansWrapper = builder.build();
        TemplateHashModel staticModels = beansWrapper.getStaticModels();
        TemplateHashModel fileStatics = null;
        try {
            fileStatics = (TemplateHashModel) staticModels.get(PaginationUtil.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fileStatics != null) {
            configuration.setSharedVariable("PaginationUtil", fileStatics);
        }
    }
}
