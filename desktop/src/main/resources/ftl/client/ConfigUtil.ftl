package ${projectPackage}.util;

import org.apache.commons.lang3.StringUtils;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtil {

    private static Properties properties = new Properties();

    static {
        try (InputStream in = ConfigUtil.class.getResourceAsStream("/config.properties");) {
            properties.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getServerUrl() {
        String serverUrl = getProperty("server.url");
        if (StringUtils.isEmpty(serverUrl)) {
            throw new RuntimeException("未配置服务器路径");
        }
        return  serverUrl;
    }

    private static String getProperty(String name) {
        return properties.getProperty(name);
    }
}
